"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const swagger_parser_1 = require("./spec-parser/swagger-parser");
exports.default = { parseSwagger: swagger_parser_1.default.parse };
