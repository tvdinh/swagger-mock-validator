<a name="1.0.0"></a>
# [1.0.0](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.31...v1.0.0) (2017-03-24)


### Bug Fixes

* the order of security requirements impacts validation results ([07a7122](https://bitbucket.org/atlassian/swagger-mock-validator/commits/07a7122))


### Features

* add opt-in analytics ([6439444](https://bitbucket.org/atlassian/swagger-mock-validator/commits/6439444)), closes [#54](https://bitbucket.org/atlassian/swagger-mock-validator/issue/54)
* rename project to swagger-mock-validator ([2594491](https://bitbucket.org/atlassian/swagger-mock-validator/commits/2594491)), closes [#16](https://bitbucket.org/atlassian/swagger-mock-validator/issue/16)


### BREAKING CHANGES

* The name of the module has changed from “@atlassian/swagger-pact-validator” to “swagger-mock-validator”.

To migrate you need to rename the dependency in your package.json and update any code that invokes this cli took from “swagger-pact-validator” to “swagger-mock-validator”.



<a name="0.0.31"></a>
## [0.0.31](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.30...v0.0.31) (2017-02-22)


### Features

* add breakdown of issues detected ([5c5075c](https://bitbucket.org/atlassian/swagger-mock-validator/commits/5c5075c)), closes [#39](https://bitbucket.org/atlassian/swagger-mock-validator/issue/39)



<a name="0.0.30"></a>
## [0.0.30](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.28...v0.0.30) (2017-02-20)


### Features

* add support for swagger base path ([1f9828c](https://bitbucket.org/atlassian/swagger-mock-validator/commits/1f9828c)), closes [#24](https://bitbucket.org/atlassian/swagger-mock-validator/issue/24)



<a name="0.0.28"></a>
## [0.0.28](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.27...v0.0.28) (2017-02-16)


### Features

* add support for yaml swagger files ([fabbe62](https://bitbucket.org/atlassian/swagger-mock-validator/commits/fabbe62)), closes [#20](https://bitbucket.org/atlassian/swagger-mock-validator/issue/20)



<a name="0.0.27"></a>
## [0.0.27](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.26...v0.0.27) (2017-02-08)


### Features

* add validation for auth headers ([a9cd312](https://bitbucket.org/atlassian/swagger-mock-validator/commits/a9cd312)), closes [#53](https://bitbucket.org/atlassian/swagger-mock-validator/issue/53)



<a name="0.0.26"></a>
## [0.0.26](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.25...v0.0.26) (2017-01-24)


### Features

* add validation for response content type ([1a2d1fd](https://bitbucket.org/atlassian/swagger-mock-validator/commits/1a2d1fd)), closes [#52](https://bitbucket.org/atlassian/swagger-mock-validator/issue/52)



<a name="0.0.25"></a>
## [0.0.25](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.24...v0.0.25) (2017-01-23)


### Features

* add validation for swagger consumes ([660c618](https://bitbucket.org/atlassian/swagger-mock-validator/commits/660c618)), closes [#34](https://bitbucket.org/atlassian/swagger-mock-validator/issue/34)
* add validation for swagger produces ([2a341ab](https://bitbucket.org/atlassian/swagger-mock-validator/commits/2a341ab)), closes [#33](https://bitbucket.org/atlassian/swagger-mock-validator/issue/33)



<a name="0.0.24"></a>
## [0.0.24](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.23...v0.0.24) (2017-01-17)


### Bug Fixes

* correctly extract provider state from pact interactions ([d4dd4fe](https://bitbucket.org/atlassian/swagger-mock-validator/commits/d4dd4fe))



<a name="0.0.23"></a>
## [0.0.23](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.22...v0.0.23) (2017-01-16)


### Features

* add validation for query parameters ([774cef3](https://bitbucket.org/atlassian/swagger-mock-validator/commits/774cef3)), closes [#8](https://bitbucket.org/atlassian/swagger-mock-validator/issue/8)



<a name="0.0.22"></a>
## [0.0.22](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.21...v0.0.22) (2017-01-12)


### Features

* add pact broker support ([b2e1deb](https://bitbucket.org/atlassian/swagger-mock-validator/commits/b2e1deb)), closes [#10](https://bitbucket.org/atlassian/swagger-mock-validator/issue/10)



<a name="0.0.21"></a>
## [0.0.21](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.20...v0.0.21) (2017-01-06)


### Features

* add validation for additional property schemas ([e4967b9](https://bitbucket.org/atlassian/swagger-mock-validator/commits/e4967b9)), closes [#35](https://bitbucket.org/atlassian/swagger-mock-validator/issue/35)



<a name="0.0.20"></a>
## [0.0.20](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.19...v0.0.20) (2017-01-04)


### Bug Fixes

* remove default parameters for node 4.x compatibility ([ddbdb45](https://bitbucket.org/atlassian/swagger-mock-validator/commits/ddbdb45)), closes [#50](https://bitbucket.org/atlassian/swagger-mock-validator/issue/50)



<a name="0.0.19"></a>
## [0.0.19](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.18...v0.0.19) (2017-01-03)


### Features

* add validation for type array ([96b44a9](https://bitbucket.org/atlassian/swagger-mock-validator/commits/96b44a9)), closes [#49](https://bitbucket.org/atlassian/swagger-mock-validator/issue/49)



<a name="0.0.18"></a>
## [0.0.18](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.17...v0.0.18) (2016-12-30)


### Features

* add validation for enum ([e83600e](https://bitbucket.org/atlassian/swagger-mock-validator/commits/e83600e)), closes [#40](https://bitbucket.org/atlassian/swagger-mock-validator/issue/40)
* add validation for maximum and exclusiveMaximum ([8371cc2](https://bitbucket.org/atlassian/swagger-mock-validator/commits/8371cc2)), closes [#41](https://bitbucket.org/atlassian/swagger-mock-validator/issue/41)
* add validation for maxLength and minLength ([8aa82e8](https://bitbucket.org/atlassian/swagger-mock-validator/commits/8aa82e8)), closes [#45](https://bitbucket.org/atlassian/swagger-mock-validator/issue/45) [#46](https://bitbucket.org/atlassian/swagger-mock-validator/issue/46)
* add validation for minimum and exclusiveMinimum ([11e7eba](https://bitbucket.org/atlassian/swagger-mock-validator/commits/11e7eba)), closes [#43](https://bitbucket.org/atlassian/swagger-mock-validator/issue/43)
* add validation for multipleOf ([bfe149e](https://bitbucket.org/atlassian/swagger-mock-validator/commits/bfe149e)), closes [#48](https://bitbucket.org/atlassian/swagger-mock-validator/issue/48)
* add validation for pattern ([d20251e](https://bitbucket.org/atlassian/swagger-mock-validator/commits/d20251e)), closes [#47](https://bitbucket.org/atlassian/swagger-mock-validator/issue/47)



<a name="0.0.17"></a>
## [0.0.17](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.16...v0.0.17) (2016-12-29)


### Features

* add validation for swagger formats ([5bab1fd](https://bitbucket.org/atlassian/swagger-mock-validator/commits/5bab1fd)), closes [#29](https://bitbucket.org/atlassian/swagger-mock-validator/issue/29)



<a name="0.0.16"></a>
## [0.0.16](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.15...v0.0.16) (2016-12-19)


### Features

* add counts of errors and warnings detected ([0846030](https://bitbucket.org/atlassian/swagger-mock-validator/commits/0846030)), closes [#38](https://bitbucket.org/atlassian/swagger-mock-validator/issue/38)
* add validation for response headers ([2218cb3](https://bitbucket.org/atlassian/swagger-mock-validator/commits/2218cb3)), closes [#5](https://bitbucket.org/atlassian/swagger-mock-validator/issue/5)



<a name="0.0.15"></a>
## [0.0.15](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.14...v0.0.15) (2016-12-14)


### Features

* display warnings when validation succeeds ([1be61b5](https://bitbucket.org/atlassian/swagger-mock-validator/commits/1be61b5))



<a name="0.0.14"></a>
## [0.0.14](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.13...v0.0.14) (2016-12-14)


### Features

* add validation for request headers ([2676b27](https://bitbucket.org/atlassian/swagger-mock-validator/commits/2676b27)), closes [#2](https://bitbucket.org/atlassian/swagger-mock-validator/issue/2) [#7](https://bitbucket.org/atlassian/swagger-mock-validator/issue/7)
* undefined non-standard request headers are now warnings ([adca8bd](https://bitbucket.org/atlassian/swagger-mock-validator/commits/adca8bd))



<a name="0.0.13"></a>
## [0.0.13](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.12...v0.0.13) (2016-12-07)


### Features

* add support for default swagger responses ([a391c8b](https://bitbucket.org/atlassian/swagger-mock-validator/commits/a391c8b)), closes [#30](https://bitbucket.org/atlassian/swagger-mock-validator/issue/30)



<a name="0.0.12"></a>
## [0.0.12](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.11...v0.0.12) (2016-12-05)


### Features

* add validation for response body ([ac2c479](https://bitbucket.org/atlassian/swagger-mock-validator/commits/ac2c479)), closes [#6](https://bitbucket.org/atlassian/swagger-mock-validator/issue/6)
* reduce verbosity of the output when errors are detected ([d300546](https://bitbucket.org/atlassian/swagger-mock-validator/commits/d300546)), closes [#27](https://bitbucket.org/atlassian/swagger-mock-validator/issue/27)



<a name="0.0.11"></a>
## [0.0.11](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.10...v0.0.11) (2016-11-30)


### Features

* add validation for response status ([6a1feec](https://bitbucket.org/atlassian/swagger-mock-validator/commits/6a1feec)), closes [#4](https://bitbucket.org/atlassian/swagger-mock-validator/issue/4)


### Performance Improvements

* improve validation speed for large swagger files ([e009399](https://bitbucket.org/atlassian/swagger-mock-validator/commits/e009399))



<a name="0.0.10"></a>
## [0.0.10](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.9...v0.0.10) (2016-11-24)


### Features

* add validation for json request bodies ([7dfb267](https://bitbucket.org/atlassian/swagger-mock-validator/commits/7dfb267)), closes [#22](https://bitbucket.org/atlassian/swagger-mock-validator/issue/22)



<a name="0.0.9"></a>
## [0.0.9](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.8...v0.0.9) (2016-11-21)


### Bug Fixes

* partial path matching causing exceptions ([8507ff5](https://bitbucket.org/atlassian/swagger-mock-validator/commits/8507ff5))



<a name="0.0.8"></a>
## [0.0.8](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.7...v0.0.8) (2016-11-18)


### Bug Fixes

* swagger validation warnings treated as warnings not errors ([ccf7251](https://bitbucket.org/atlassian/swagger-mock-validator/commits/ccf7251)), closes [#21](https://bitbucket.org/atlassian/swagger-mock-validator/issue/21) [#19](https://bitbucket.org/atlassian/swagger-mock-validator/issue/19)



<a name="0.0.7"></a>
## [0.0.7](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.6...v0.0.7) (2016-11-17)


### Bug Fixes

* handle undefined path parameters when method is missing ([b595073](https://bitbucket.org/atlassian/swagger-mock-validator/commits/b595073))



<a name="0.0.6"></a>
## [0.0.6](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.5...v0.0.6) (2016-11-17)


### Features

* add validation for request methods ([778b8e0](https://bitbucket.org/atlassian/swagger-mock-validator/commits/778b8e0)), closes [#3](https://bitbucket.org/atlassian/swagger-mock-validator/issue/3)



<a name="0.0.5"></a>
## [0.0.5](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.4...v0.0.5) (2016-11-08)



<a name="0.0.4"></a>
## [0.0.4](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.3...v0.0.4) (2016-09-13)


### Features

* add support for retrieving pact and swagger files via urls ([ab248dc](https://bitbucket.org/atlassian/swagger-mock-validator/commits/ab248dc))



<a name="0.0.3"></a>
## [0.0.3](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.2...v0.0.3) (2016-09-01)


### Bug Fixes

* path parameter parser no longer case sensitive for request method ([cab9c81](https://bitbucket.org/atlassian/swagger-mock-validator/commits/cab9c81))



<a name="0.0.2"></a>
## [0.0.2](https://bitbucket.org/atlassian/swagger-mock-validator/compare/0.0.1...v0.0.2) (2016-08-24)


### Features

* add validation for request paths ([d95aa8f](https://bitbucket.org/atlassian/swagger-mock-validator/commits/d95aa8f)), closes [#1](https://bitbucket.org/atlassian/swagger-mock-validator/issue/1)



