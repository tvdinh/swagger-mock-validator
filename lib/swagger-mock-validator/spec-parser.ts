import swaggerParser from './spec-parser/swagger-parser';

export default {parseSwagger: swaggerParser.parse};
