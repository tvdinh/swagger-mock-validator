declare module 'util' {
    export interface InspectOptions {
        breakLength?: number;
    }
}
